{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };
        libPath = with pkgs; lib.makeLibraryPath [
          libGL
          libxkbcommon
          wayland
          fontconfig
          xorg.libX11
          xorg.libXcursor
          xorg.libXi
          xorg.libXrandr
          openssl

        ];
        manifest = (pkgs.lib.importTOML ./Cargo.toml).package;
        potacat-pkg = pkgs.rustPlatform.buildRustPackage rec {
          pname = manifest.name;
          version = manifest.version;
          cargoLock.lockFile = ./Cargo.lock;
          src = pkgs.lib.cleanSource ./.;
          nativeBuildInputs = with pkgs; [ cmake pkg-config makeWrapper ];
          buildInputs = with pkgs; [ openssl ];
          cargoSha256 = pkgs.lib.fakeSha256;
          postInstall = ''
           wrapProgram $out/bin/potacat --prefix LD_LIBRARY_PATH : ${libPath}
          '';
        };

      in
      {
        defaultPackage = potacat-pkg;
        devShell = with pkgs; mkShell {
          inputsFrom = [ potacat-pkg ];
          buildInputs = [ hamlib cargo rust-analyzer rustc rustfmt pre-commit rustPackages.clippy ];
          LD_LIBRARY_PATH = libPath;
        };
      }
    );
}
