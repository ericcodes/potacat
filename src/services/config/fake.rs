use anyhow::Result;
use log::debug;

use crate::types::FrequencyHz;

use super::{BandConfig, Config};

pub struct API {
    bands: Vec<BandConfig>,
}

pub fn new() -> API {
    API {
        bands: vec![
            BandConfig {
                band: "40m".to_string(),
                start_hz: 7000000,
                stop_hz: 7300000,
                mode: "SSB".to_string(),
                rig_mode: "LSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "40m".to_string(),
                start_hz: 7000000,
                stop_hz: 7300000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "40m".to_string(),
                start_hz: 7000000,
                stop_hz: 7300000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "20m".to_string(),
                start_hz: 14000000,
                stop_hz: 14350000,
                mode: "SSB".to_string(),
                rig_mode: "USB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "20m".to_string(),
                start_hz: 14000000,
                stop_hz: 14350000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "20m".to_string(),
                start_hz: 14000000,
                stop_hz: 14350000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "17m".to_string(),
                start_hz: 18068000,
                stop_hz: 18168000,
                mode: "SSB".to_string(),
                rig_mode: "USB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "17m".to_string(),
                start_hz: 18068000,
                stop_hz: 18168000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "17m".to_string(),
                start_hz: 18068000,
                stop_hz: 18168000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "15m".to_string(),
                start_hz: 21000000,
                stop_hz: 21450000,
                mode: "SSB".to_string(),
                rig_mode: "USB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "15m".to_string(),
                start_hz: 21000000,
                stop_hz: 21450000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "15m".to_string(),
                start_hz: 21000000,
                stop_hz: 21450000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "12m".to_string(),
                start_hz: 24890000,
                stop_hz: 24990000,
                mode: "SSB".to_string(),
                rig_mode: "USB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "12m".to_string(),
                start_hz: 24890000,
                stop_hz: 24990000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "12m".to_string(),
                start_hz: 24890000,
                stop_hz: 24990000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "10m".to_string(),
                start_hz: 28000000,
                stop_hz: 29000000,
                mode: "SSB".to_string(),
                rig_mode: "USB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "10m".to_string(),
                start_hz: 28000000,
                stop_hz: 29000000,
                mode: "FT8".to_string(),
                rig_mode: "PKTUSB".to_string(),
                rig_passband: -1,
            },
            BandConfig {
                band: "10m".to_string(),
                start_hz: 28000000,
                stop_hz: 29000000,
                mode: "CW".to_string(),
                rig_mode: "CW".to_string(),
                rig_passband: -1,
            },
        ],
    }
}

impl Config for API {
    fn find_band_config_for_rig(
        &self,
        hz: FrequencyHz,
        rig_mode: &str,
    ) -> Result<Option<BandConfig>> {
        for band in self.bands.iter() {
            debug!("{:?} {:?} {:?}", band, hz, rig_mode);
            if hz < band.start_hz {
                debug!("{} < {}", hz, band.start_hz);
                continue;
            }

            if hz > band.stop_hz {
                debug!("{} > {}", hz, band.stop_hz);
                continue;
            }

            if band.rig_mode != *rig_mode {
                debug!("{} != {}", band.rig_mode, rig_mode);
                continue;
            }

            return Ok(Some(band.clone()));
        }
        Ok(None)
    }

    fn find_band_config_for_spot(&self, hz: FrequencyHz, mode: &str) -> Result<Option<BandConfig>> {
        for band in self.bands.iter() {
            debug!("{:?} {:?} {:?}", band, hz, mode);
            if hz < band.start_hz {
                debug!("{} < {}", hz, band.start_hz);
                continue;
            }

            if hz > band.stop_hz {
                debug!("{} > {}", hz, band.stop_hz);
                continue;
            }

            if band.mode != *mode {
                debug!("{} != {}", band.mode, mode);
                continue;
            }

            return Ok(Some(band.clone()));
        }
        Ok(None)
    }
}
