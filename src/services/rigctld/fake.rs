// This provides a fake implementation of the Rigctld interface. This
// allows me to tweak the UI without a running rigctld service.j
use std::cell::RefCell;

use crate::services::rigctld::{ModeResult, Rigctld};
use crate::types::FrequencyHz;
use anyhow::Result;

use super::Passband;

#[derive(Debug, PartialEq)]
pub struct API {
    pub frequency: RefCell<FrequencyHz>,
    pub mode: RefCell<ModeResult>,
}

impl API {
    pub fn new() -> Self {
        API {
            frequency: RefCell::new(14420000),
            mode: RefCell::new(ModeResult {
                mode: "USB".to_string(),
                passband: 1800,
            }),
        }
    }
}

impl Rigctld for API {
    fn set_frequency(&self, frequency: FrequencyHz) -> Result<()> {
        self.frequency.replace(frequency);
        Ok(())
    }

    fn get_frequency(&self) -> Result<FrequencyHz> {
        let freq = self.frequency.borrow();
        Ok(*freq)
    }

    fn set_mode(&self, mode: &str, passband: Passband) -> Result<()> {
        let mode = ModeResult {
            mode: mode.to_owned(),
            passband,
        };
        self.mode.replace(mode);
        Ok(())
    }

    fn get_mode(&self) -> Result<ModeResult> {
        let mode = self.mode.borrow();
        Ok(mode.to_owned())
    }
}
