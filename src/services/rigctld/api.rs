// This is the live implementation of the Rigctld. For simplicity
// sake, it shells out and runs the rigctl command and parses its
// output.
use std::process::{Command, Output};

use crate::{
    services::rigctld::{ModeResult, Rigctld},
    types::FrequencyHz,
};
use anyhow::{anyhow, Result};

use super::Passband;

pub struct API {
    address: String,
}

impl API {
    pub fn new(host: &str, port: u16) -> Self {
        API {
            address: format!("{}:{}", host, port),
        }
    }
}

fn output_to_error(output: Output) -> anyhow::Error {
    let stderr = match String::from_utf8(output.stderr) {
        Ok(x) => x,
        Err(e) => return anyhow!(e),
    };
    anyhow!(stderr)
}

impl Rigctld for API {
    fn set_frequency(&self, frequency: FrequencyHz) -> Result<()> {
        let output = Command::new("rigctl")
            .args(["-m", "2", "-r", &self.address, "F", &frequency.to_string()])
            .output()?;

        if !output.status.success() {
            return Err(output_to_error(output));
        }

        Ok(())
    }

    fn get_frequency(&self) -> Result<FrequencyHz> {
        let output = Command::new("rigctl")
            .args(["-m", "2", "-r", &self.address, "f"])
            .output()?;

        if !output.status.success() {
            return Err(output_to_error(output));
        }

        let freq = String::from_utf8(output.stdout)?;
        let freq = freq.trim().parse::<u64>()?;

        Ok(freq)
    }

    fn set_mode(&self, mode: &str, passband: Passband) -> Result<()> {
        let output = Command::new("rigctl")
            .args([
                "-m",
                "2",
                "-r",
                &self.address,
                "M",
                &mode.to_string(),
                &passband.to_string(),
            ])
            .output()?;

        if !output.status.success() {
            return Err(output_to_error(output));
        }

        Ok(())
    }

    fn get_mode(&self) -> Result<ModeResult> {
        let output = Command::new("rigctl")
            .args(["-m", "2", "-r", &self.address, "m"])
            .output()?;

        if !output.status.success() {
            return Err(output_to_error(output));
        }
        let output = String::from_utf8(output.stdout)?;
        let mut lines = output.lines();
        let mode = lines
            .next()
            .ok_or_else(|| anyhow!("Unexpected end of output when reading mode"))?
            .to_string();
        let passband = lines
            .next()
            .ok_or_else(|| anyhow!("Unexpected end of output when reading frequency"))?;

        let passband = passband.parse::<i64>()?;

        Ok(ModeResult { mode, passband })
    }
}
