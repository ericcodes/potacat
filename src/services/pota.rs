// This provides the POTA.app interface definition and types
pub mod api;
pub mod fake;

use anyhow::Result;

use crate::types::FrequencyHz;

#[derive(Clone, Debug)]
pub struct Park {
    pub reference: String,
    pub grid_square: String,
    pub name: String,
    pub location: String,
}

pub type Band = String;
pub type Mode = String;
pub type Program = String;

#[derive(Clone, Debug)]
pub struct Spot {
    pub spot_id: String,
    pub callsign: String,
    pub park: Park,
    pub mode: Mode,
    pub frequency: FrequencyHz,
}

pub trait PotaService {
    fn spots(&self) -> Result<Vec<Spot>>;
}
