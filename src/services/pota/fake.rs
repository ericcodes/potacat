// This is a fake implementation of the POTA api. This lets me tweak
// the UI without hitting the POTA service.
use crate::services::pota::*;
use anyhow::{anyhow, Result};

pub struct API {
    pub spots_data: core::result::Result<Vec<Spot>, String>,
}

impl API {
    pub fn new() -> Self {
        Self {
            spots_data: Ok(vec![
                Spot {
                    spot_id: "1".into(),
                    callsign: "AC0AB".into(),
                    park: Park {
                        reference: "US-2958".into(),
                        grid_square: "EM66ic".into(),
                        name: "Montgomery Bell State Park".into(),
                        location: "US-TN".into(),
                    },
                    mode: "SSB".into(),
                    frequency: 14240000,
                },
                Spot {
                    spot_id: "2".into(),
                    callsign: "AC0AB".into(),
                    park: Park {
                        reference: "US-2958".into(),
                        grid_square: "EM66ic".into(),
                        name: "Montgomery Bell State Park".into(),
                        location: "US-TN".into(),
                    },
                    mode: "CW".into(),
                    frequency: 7240000,
                },
                Spot {
                    spot_id: "3".into(),
                    callsign: "AC0AB".into(),
                    park: Park {
                        reference: "US-2958".into(),
                        grid_square: "EM66ic".into(),
                        name: "Montgomery Bell State Park".into(),
                        location: "US-TN".into(),
                    },
                    mode: "FT8".into(),
                    frequency: 7074000,
                },
            ]),
        }
    }
}

impl PotaService for API {
    fn spots(&self) -> Result<Vec<Spot>> {
        let data = self
            .spots_data
            .as_ref()
            .map(Vec::clone)
            .map_err(|err| anyhow!(err.clone()))?;

        Ok(data)
    }
}
