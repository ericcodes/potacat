// This is the live implementation of the POTA API. It hits
// https://api.pota.app/spot/activator and parses the data for the
// application.
use log::error;
use serde::Deserialize;

use crate::{services::pota::PotaService, types::FrequencyHz};

use super::Park;
use super::Spot;

pub struct API {}

impl API {
    pub fn new() -> Self {
        Self {}
    }
}

#[derive(Debug, Deserialize)]
struct Activator {
    #[serde(rename = "spotId")]
    spot_id: u64,
    activator: String,
    frequency: String,
    mode: String,
    reference: String,
    name: String,
    grid6: String,
    #[serde(rename = "locationDesc")]
    location_desc: String,
}

impl Into<Spot> for Activator {
    fn into(self) -> Spot {
        let freq_hz: FrequencyHz = self
            .frequency
            .parse::<f64>()
            .map(|f| (f * 1000.0))
            .map(|f| f as u64)
            .unwrap_or_else(|e| {
                error!("could not parse {:?} as f64: {:?}", self.frequency, e);
                0
            });

        Spot {
            spot_id: self.spot_id.to_string(),
            callsign: self.activator,
            park: Park {
                reference: self.reference,
                grid_square: self.grid6,
                name: self.name,
                location: self.location_desc,
            },
            frequency: freq_hz,
            mode: self.mode,
        }
    }
}

impl PotaService for API {
    fn spots(&self) -> anyhow::Result<Vec<super::Spot>> {
        let activators: Vec<Activator> =
            reqwest::blocking::get("https://api.pota.app/spot/activator")?.json()?;
        Ok(activators
            .into_iter()
            .map(Into::into)
            .collect::<Vec<Spot>>())
    }
}
