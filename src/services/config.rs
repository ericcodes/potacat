use crate::services::rigctld::Passband;
use crate::types::FrequencyHz;
use anyhow::Result;

pub mod fake;

/// config is the configuration service the manages the application configuration

pub trait Config {
    /// find_band_config_for_rig finds the correct band config for a rig's mode
    fn find_band_config_for_rig(
        &self,
        hz: FrequencyHz,
        rig_mode: &str,
    ) -> Result<Option<BandConfig>>;

    /// find_band_config_for_spot finds the correct band config for a pota spot
    fn find_band_config_for_spot(&self, hz: FrequencyHz, mode: &str) -> Result<Option<BandConfig>>;
}

#[derive(Debug, Clone)]
pub struct BandConfig {
    // The band name that POTA uses, 20m, 40m, etc
    pub band: String,

    // The start frequency for the band
    pub start_hz: FrequencyHz,

    // The stop frequency for the band
    pub stop_hz: FrequencyHz,

    // The mode_id that POTA uses to ID the mode
    pub mode: String,

    // The rigctl mode to use when setting the mode
    pub rig_mode: String,

    // The rigctl passband to use when setting the mode
    pub rig_passband: Passband,
}
