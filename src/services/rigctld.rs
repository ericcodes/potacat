// This provides the interface definition for rigctld
pub mod api;
pub mod fake;

use crate::types::FrequencyHz;
use anyhow::Result;

pub type Passband = i64;

#[derive(Clone, PartialEq, Debug)]
pub struct ModeResult {
    pub mode: String,
    pub passband: Passband,
}

pub trait Rigctld {
    fn set_frequency(&self, frequency: FrequencyHz) -> Result<()>;
    fn get_frequency(&self) -> Result<FrequencyHz>;
    fn set_mode(&self, mode: &str, passband: Passband) -> Result<()>;
    fn get_mode(&self) -> Result<ModeResult>;
}
