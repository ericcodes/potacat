use structopt::StructOpt;

pub mod services;
pub mod types;
pub mod ui;

#[derive(Debug, StructOpt)]
struct Opt {
    /// Activate demo mode
    #[structopt(short, long)]
    pub demo: bool,
}

fn main() {
    env_logger::init();
    let opt = Opt::from_args();

    let handles = if !opt.demo {
        ui::Handles {
            config: Box::new(services::config::fake::new()),
            rig: Box::new(services::rigctld::api::API::new("127.0.0.1", 4532)),
            pota: Box::new(services::pota::api::API::new()),
        }
    } else {
        ui::Handles {
            config: Box::new(services::config::fake::new()),
            rig: Box::new(services::rigctld::fake::API::new()),
            pota: Box::new(services::pota::fake::API::new()),
        }
    };

    ui::main(handles).expect("no error")
}
