// This is the egui UI for the application.  The architecture is
// pretty simple.
//
// The main thread controls the UI
//
// There is a second controller thread the polls the rigctl for its
// frequency and fetches new spot data when asked. The controller owns
// the service interfaces.
//
// The two threads communicate with each other over channels.
use std::{
    sync::mpsc::{channel, Receiver, Sender},
    thread,
    time::Duration,
};

use crate::{
    services::{
        config::Config,
        pota::{self, PotaService, Spot},
        rigctld::{ModeResult, Rigctld},
    },
    types::FrequencyHz,
};
use anyhow::Result;
use eframe::egui;
use log::{debug, error, info};

pub struct Handles {
    pub config: Box<dyn Config + Send>,
    pub pota: Box<dyn PotaService + Send>,
    pub rig: Box<dyn Rigctld + Send>,
}

pub fn main(handles: Handles) -> Result<()> {
    let spots = Vec::<Spot>::new();
    let frequency = 0;

    let options = eframe::NativeOptions {
        viewport: egui::ViewportBuilder::default().with_inner_size([320.0, 240.0]),
        ..Default::default()
    };

    let (app_receiver, controller_sender) = spawn_controller(handles);

    // prime the controller with a refresh message
    controller_sender
        .send(ControllerMessage::RefreshSpots)
        .unwrap_or_else(|e| error!("error sending first refresh message: {:?}", e));

    let app = Box::new(MyApp {
        spots,
        mode: "???".to_string(),
        frequency,
        app_receiver,
        controller_sender,
    });

    eframe::run_native("potacat", options, Box::new(|_cc| app)).expect("error with egui");
    Ok(())
}

#[derive(Debug)]
enum AppMessage {
    UpdateFrequency(Result<FrequencyHz>),
    UpdateMode(Result<ModeResult>),
    UpdateSpots(Result<Vec<Spot>>),
}

#[derive(Debug)]
enum ControllerMessage {
    SetFrequency(FrequencyHz, pota::Mode),
    RefreshSpots,
}

fn handle_message(
    handles: &Handles,
    app_sender: Sender<AppMessage>,
    msg: &ControllerMessage,
) -> Result<()> {
    debug!("got ControllerMessage: {:?}", msg);

    match msg {
        ControllerMessage::SetFrequency(freq, mode) => {
            info!("setting frequency to {}", freq);
            handles.rig.set_frequency(*freq)?;

            info!("setting mode to {}", mode);
            match handles.config.find_band_config_for_spot(*freq, &mode)? {
                Some(cfg) => {
                    info!("setting rig to {:?}", cfg);
                    handles.rig.set_mode(&cfg.rig_mode, cfg.rig_passband)?;
                }
                None => {
                    info!(
                        "no rig mode config found for POTA mode {}, skipping mode change",
                        &mode
                    );
                }
            };
        }

        ControllerMessage::RefreshSpots => {
            info!("refreshing spots");
            let spots: Result<Vec<Spot>> = handles.pota.spots().map(|spots| {
                spots
                    .into_iter()
                    .filter(|spot| {
                        handles
                            .config
                            .find_band_config_for_spot(spot.frequency, &spot.mode)
                            .unwrap_or_else(|e| {
                                error!("Error getting config for spot: {:?}", e);
                                None
                            })
                            .is_some()
                    })
                    .collect()
            });
            app_sender.send(AppMessage::UpdateSpots(spots))?;
        }
    }
    Ok(())
}

fn spawn_controller(handles: Handles) -> (Receiver<AppMessage>, Sender<ControllerMessage>) {
    let (app_sender, app_receiver) = channel();
    let (controller_sender, controller_receiver) = channel();

    thread::spawn(move || -> ! {
        loop {
            // process controller messages
            for msg in controller_receiver.try_iter() {
                handle_message(&handles, app_sender.clone(), &msg).unwrap_or_else(|err| {
                    error!("Error handling msg: {:?}: {:?}", &msg, err);
                });
            }

            // always update the frequency
            let freq = handles.rig.get_frequency();
            app_sender
                .send(AppMessage::UpdateFrequency(freq))
                .unwrap_or_else(|e| error!("error sending latest frequency: {:?}", e));

            let mode = handles.rig.get_mode();
            app_sender
                .send(AppMessage::UpdateMode(mode))
                .unwrap_or_else(|e| error!("error sending latest rig mode: {:?}", e));

            thread::sleep(Duration::from_secs(1));
        }
    });

    // Send back the handles for the GUI thread to use for
    // communication
    (app_receiver, controller_sender)
}

// MyApp is the apps state
struct MyApp {
    // The current spots
    spots: Vec<Spot>,
    // The rig's current frequency
    frequency: FrequencyHz,

    // The rig's current mode
    mode: String,

    // The channel to receive messages from the controller thread
    app_receiver: Receiver<AppMessage>,
    // The channel to send messages to the controller thread
    controller_sender: Sender<ControllerMessage>,
}

impl MyApp {
    // What a spot looks like
    fn spot_widget(&self, ui: &mut egui::Ui, rig_hz: FrequencyHz, spot: &Spot) {
        // render a human-readable MHz version of the spot's frequency
        let freq = &freq_display(spot.frequency);
        let selected_label = if spot.frequency == rig_hz { "> " } else { "" };

        // The callsign and park reference label
        ui.label(format!(
            "{}{} @ {}",
            selected_label, spot.callsign, spot.park.reference
        ));

        // Create a 2 column grid for the properties of the spot
        egui::Grid::new(spot.spot_id.clone())
            .num_columns(2)
            .spacing([40.0, 2.0])
            .striped(true)
            .show(ui, |ui| {
                // Display the Frequency of the spot
                ui.label("Freq");
                if ui.button(freq).clicked() {
                    self.controller_sender
                        .send(ControllerMessage::SetFrequency(
                            spot.frequency,
                            spot.mode.clone(),
                        ))
                        .unwrap_or_else(|e| error!("error sending set frequency message: {:?}", e));
                }

                ui.end_row();

                ui.label("Mode");
                ui.label(&spot.mode);

                ui.end_row();

                // Display the Park's name
                ui.label("Park");
                ui.label(format!("{}", spot.park.name));

                ui.end_row();

                // Display the Park's name
                ui.label("Location");
                ui.label(format!("{}", spot.park.location));

                ui.end_row();

                // The Park's grid square
                ui.label("Grid");
                ui.label(format!("{}", spot.park.grid_square));
            });
    }
}

// This is the egui App widget
impl eframe::App for MyApp {
    fn update(&mut self, ctx: &egui::Context, _frame: &mut eframe::Frame) {
        // schedule a repaint so that we're never more that 100ms away
        // from a new message this is a bit of a hack as I don't know
        // how to update the GUI when a message is ready.
        ctx.request_repaint_after(Duration::from_millis(100));

        // read any pending app messages
        for msg in self.app_receiver.try_iter() {
            debug!("got AppMessage: {:?}", msg);
            match msg {
                AppMessage::UpdateFrequency(Ok(freq)) => {
                    info!("got latest frequency from rig: {:?}", freq);
                    self.frequency = freq;
                }
                AppMessage::UpdateFrequency(Err(e)) => {
                    error!("error getting the latest frequency: {:?}", e);
                }
                AppMessage::UpdateMode(Ok(mode)) => {
                    info!("got latest mode from rig: {:?}", mode);
                    self.mode = mode.mode;
                }
                AppMessage::UpdateMode(Err(e)) => {
                    error!("error get the latest mode: {:?}", e);
                }
                AppMessage::UpdateSpots(Ok(mut spots)) => {
                    info!("got the latest spots from POTA.app: {:?}", spots.len());
                    spots.sort_by_key(|s| s.frequency);
                    self.spots = spots;
                }
                AppMessage::UpdateSpots(Err(e)) => {
                    error!("error getting the latest spots: {:?}", e);
                }
            }
        }

        // Render the app window
        egui::CentralPanel::default().show(ctx, |ui| {
            // The App header
            ui.heading("POTA CAT, Meow");

            // The current frequency as far as we know
            ui.label(format!(
                "Frequency: {}; Mode: {}",
                freq_display(self.frequency),
                self.mode,
            ));
            if ui.button("refresh").clicked() {
                self.controller_sender
                    .send(ControllerMessage::RefreshSpots)
                    .unwrap_or_else(|e| error!("error sending refresh on button: {:?}", e))
            }

            // TODO: Add a form for filtering and sorting the spots

            // A scrollable area for the spots
            egui::ScrollArea::vertical().show(ui, |ui| {
                // Render each spot in the scroll area
                for spot in &self.spots {
                    self.spot_widget(ui, self.frequency, spot);
                }
            })
        });
    }
}

fn freq_display(f: FrequencyHz) -> String {
    let mhz = (f as f64) / 1_000_000.0;
    format!("{:.3}", mhz)
}
