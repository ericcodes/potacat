# POTA CAT meow

This is a little application that helps me hunt POTA. It fetches the latest spots from https://pota.app/ and lets me quickly tune to a frequency using hamlib.

## Usage

First start `rigctld` for your radio. Then run the app. This expects that `rigctl` is in your path.

